package kz.aitu.oop.practice.practice4;

import java.util.ArrayList;
import java.util.List;

public class Reptiles extends Aquarium {
    private List<String> reptilesType = new ArrayList<>();          //создания List

    public Reptiles(int size, int cost) {                           //вызываем конструктор который создали в классе аквариум
        super(size, cost);
    }

    public void totalCost() {                                       //считаем цену
        System.out.printf("Total cost equal to %s$.\n", cost);
    }

    public void add(String fish) {                                  //добавляем рептилии в List
        reptilesType.add(fish);
        System.out.println(fish + " is added in aquarium.");
        cost += 6;
    }

    public void remove(String fish) {                              //удаляем рептилии в List
        reptilesType.remove(fish);
        cost -= 6;
    }

    public void show() {                                           //показать всех рептилии
        System.out.println(reptilesType.size() + " reptiles in shopping basket");
        reptilesType.forEach(fish -> System.out.println(fish));
    }

    @Override
    public void checkKindOfAnimals() {
        System.out.println("The aquarium is for Reptiles");
    }

    @Override
    public void consistWater() {
        System.out.println("There is no water in the aquarium.");
    }
}
