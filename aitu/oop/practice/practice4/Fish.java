package kz.aitu.oop.practice.practice4;

import java.util.ArrayList;
import java.util.List;

public class Fish extends Aquarium {
    private List<String> fishType = new ArrayList<>();      //создания List

    public Fish(int size, int cost) {                       //вызываем конструктор который создали в классе аквариум
        super(size, cost);
    }

    public void totalCost() {                               //считаем цену
        System.out.printf("Total cost equal to %s$.\n", cost);
    }

    public void add(String fish) {                          //добавляем рыб в List
        fishType.add(fish);
        System.out.println(fish + " is added in aquarium.");
        cost += 4;
    }

    public void remove(String fish) {                       //удаляем рыб в List
        fishType.remove(fish);
        cost -= 4;
    }

    public void show() {                                    //показать всех рыб
        System.out.println(fishType.size() + " fish in shopping basket");
        fishType.forEach(fish -> System.out.println(fish));
    }

    @Override
    public void checkKindOfAnimals() {
        System.out.println("The aquarium is for Fish");
    }

    @Override
    public void consistWater() {
        System.out.printf("There is water in the aquarium. And also %s l of water aquarium consist.\n", getSize());
    }
}
