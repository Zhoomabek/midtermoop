package kz.aitu.oop.practice.practice4;

abstract class Aquarium {
    private int size;            // Размер аквариума
    public int cost;             // цена
    public final int stone = 4;     //цена камней
    public final int coral = 6;     //цена карал
    public final int plants = 2;    //цена растений

    public Aquarium(int size, int cost) {   //конструктор для size и cost
        this.size = size;
        this.cost = cost;
    }

    public int getCost() {                  //герттер и сеттер для size и cost
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }


    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void accessories(int stoneN, int coralN, int plantsN) {      //добавлять цену аксессуаров
        cost += stoneN * stone + coralN * coral + plantsN * plants;
    }

    public void add(String animal) {                                    //добавить животное
        System.out.println("Something is added.");
    }

    public void remove(String animal) {                                 //удалить животнрое
        System.out.println("Something is removed.");
    }

    public void show() {                                                //показать животных
        System.out.println("Show animal.");
    }

    public abstract void consistWater();    //посмотреть есть ли в аквариуме вода

    public abstract void checkKindOfAnimals();  //проверить какой вид животных в аквариуме

    public abstract void totalCost();                                   //вся цена за все аквариум, животных и за аксессуары;
}
