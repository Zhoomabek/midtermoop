package kz.aitu.oop.practice.practice4;

import java.util.Scanner;

public class Aqua {
    public static int sizeCost = 3;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Hello, there you can buy accessories, animals and aquarium.");
        System.out.print("Choose type of animal reptiles or fish: ");
        String animal = scanner.next();


        switch (animal) {
            case "fish":
                System.out.print("Write size of aquarium for fish: ");

                int size1 = scanner.nextInt();

                Fish fish = new Fish(size1, size1 * sizeCost);

                fish.checkKindOfAnimals();
                fish.consistWater();

                System.out.println();

                fish.add("Red fish");               //добавляем рыб
                fish.add("Yellow fish");
                fish.add("Black fish");
                fish.add("White fish");

                System.out.println("\n---------------------------------\n");

                fish.show();
                fish.remove("Red fish");
                System.out.println("\n---------------------------------\n");
                fish.show();
                System.out.println("\n---------------------------------");
                System.out.print("\n---------------------------------\n");

                System.out.println("We have accessories like: stones, coral, plants.");
                System.out.print("Write number of stone: ");
                int stoneN1 = scanner.nextInt();
                System.out.print("Write number of coral: ");
                int coralN1 = scanner.nextInt();
                System.out.print("Write number of plant: ");
                int plantN1 = scanner.nextInt();
                fish.accessories(stoneN1, coralN1, plantN1);
                System.out.println("\n---------------------------------\n");

                fish.totalCost();

                System.out.println("\n---------------------------------\n");
                break;

            case "reptiles":
                System.out.print("Write size of aquarium for reptiles: ");

                int size2 = scanner.nextInt();

                Aquarium reptiles = new Reptiles(size2, size2 * sizeCost);

                System.out.printf("Aquarium have %s m^3 size.\n", reptiles.getSize());
                reptiles.checkKindOfAnimals();
                reptiles.consistWater();

                System.out.println();

                reptiles.add("Red turtle");                         //добавляем рептилии
                reptiles.add("Black turtle");
                reptiles.add("White turtle");

                System.out.println("\n---------------------------------\n");

                reptiles.show();
                reptiles.remove("White turtle");
                System.out.println("\n---------------------------------\n");
                reptiles.show();

                System.out.println("\n---------------------------------");
                System.out.print("\n---------------------------------\n");

                System.out.println("We have accessories like: stones, coral, plants.");

                System.out.print("Write number of stone: ");
                int stoneN2 = scanner.nextInt();
                System.out.print("Write number of coral: ");
                int coralN2 = scanner.nextInt();
                System.out.print("Write number of plant: ");
                int plantN2 = scanner.nextInt();
                reptiles.accessories(stoneN2, coralN2, plantN2);

                reptiles.totalCost();

                System.out.println("\n---------------------------------\n");
                break;

            default:
                System.out.println("Sorry, we don't have this type of animals.");
                break;

        }
    }
}
